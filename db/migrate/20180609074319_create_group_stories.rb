class CreateGroupStories < ActiveRecord::Migration[5.2]
  def change
    create_table :group_stories do |t|
      t.references :group, foreign_key: true
      t.references :story, foreign_key: true

      t.timestamps
    end
  end
end
