class AddLastActiveAtToAccounts < ActiveRecord::Migration[5.2]
  def change
    add_column :accounts, :last_active_at, :datetime
  end
end
