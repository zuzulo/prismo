import BaseController from "./base_controller"
import api from '../lib/api'

export default class extends BaseController {
  initialize () {
  }

  toggle () {
    this.opened = !this.opened
  }

  windowClicked (e) {
    if (!this.opened) { return false }

    if (this.element.contains(event.target) == false) {
      this.opened = false
    }
  }

  _handleOpen () {
    this.element.classList.add("dropdown-open")
  }

  _handleClose () {
    this.element.classList.remove("dropdown-open")
  }

  get opened () {
    return this.data.get('opened') == 'true'
  }

  set opened (value) {
    this.data.set('opened', value)

    if (value) {
      this._handleOpen()
    } else {
      this._handleClose()
    }
  }
}
