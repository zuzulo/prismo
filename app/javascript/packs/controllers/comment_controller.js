import axios from 'axios'
import BaseController from './base_controller'

export default class extends BaseController {
  static targets = [
    'voteBtn',
    'replyContainer',
    'editContainer',
    'bodyContainer',
    'actionsContainer'
  ]

  toggleVote () {
    let req = axios.post(this.voteBtnTarget.dataset.actionPath)

    req.then((resp) => {
      this._handleToggleVoteSuccess(resp)
    })
  }

  toogleClosed () {
    this.element.classList.toggle('comment--closed')
  }

  showReply () {
    let req = axios.get(this.replyContainerTarget.dataset.actionPath)

    req.then((resp) => {
      this.replyContainerTarget.innerHTML = resp.data
    })
  }

  showEdit () {
    let req = axios.get(this.editContainerTarget.dataset.actionPath)

    req.then((resp) => {
      this.element.classList.add('comment--editing')
      this.editContainerTarget.innerHTML = resp.data
    })
  }

  cancelEdit (e) {
    e.preventDefault()
    this.element.classList.remove('comment--editing')
  }

  loadNewComment (e) {
    e.preventDefault()

    let req = axios.get(e.target.getAttribute('href')),
        parser = new DOMParser(),
        id = e.target.dataset.commentId

    req.then((resp) => {
      let doc = parser.parseFromString(resp.data, "text/html"),
          incomingComment = doc.querySelector(`.comment[data-id="${id}"]`)

      e.target.parentNode.parentNode.outerHTML = incomingComment.outerHTML
    })
  }

  _handleToggleVoteSuccess (resp) {
    let parser = new DOMParser(),
        doc = parser.parseFromString(resp.data, "text/html"),
        incomingComment = doc.getElementsByClassName('comment')[0],

        currentInner = this.element.getElementsByClassName('comment-inner')[0],
        incommingInner = doc.getElementsByClassName('comment-inner')[0],

        currentClassList = this.element.classList,
        incomingClassList = incomingComment.classList

    // Take care of comment--voted class
    if (incomingClassList.contains('comment--voted')) {
      this.element.classList.add('comment--voted')
    } else {
      this.element.classList.remove('comment--voted')
    }

    // Replace comment body (everything except the child comments)
    currentInner.outerHTML = incommingInner.outerHTML
  }
}
