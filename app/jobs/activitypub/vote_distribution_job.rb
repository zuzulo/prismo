class ActivityPub::VoteDistributionJob < ApplicationJob
  queue_as :push

  def perform(vote_id)
    @vote = Vote.find(vote_id)
    @voteable = vote.voteable
    @account = vote.account

    inboxes.each do |inbox_url|
      ActivityPub::DeliveryJob.perform_later(
        signed_payload, account.id, inbox_url
      )
    end

  rescue ActiveRecord::RecordNotFound
    true
  end

  private

  attr_reader :vote, :account, :voteable

  def inboxes
    if voteable.is_a?(Comment)
      [voteable.account.inbox_url]
    else
      []
    end
  end

  def signed_payload
    @signed_payload ||= Oj.dump(ActivityPub::LinkedDataSignature.new(payload).sign!(account))
  end

  def payload
    @payload ||= ActivityPub::LikeSerializer.new(voteable, with_context: true).as_json
  end
end
