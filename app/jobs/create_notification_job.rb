class CreateNotificationJob < ApplicationJob
  queue_as :default

  def self.call(event_key, author:, recipient:, notifable: nil, context: nil)
    args = {
      event_key: event_key,
      author_id: author.id,
      author_type: author.class.name,
      recipient_id: recipient.id,
      recipient_type: recipient.class.name,
    }

    if notifable.present?
      args[:notifable_id] = notifable.id
      args[:notifable_type] = notifable.class.name
    end

    if context.present?
      args[:context_id] = context.id
      args[:context_type] = context.class.name
    end

    CreateNotificationJob.perform_later(args)
  end

  def perform(**args)
    notification = Notification.create!(args)

    NotificationsChannel.broadcast_to(
      notification.recipient.user,
      event: 'notifications.created'
    )
  end
end
