class CommentPresenter
  include ActionView::Helpers::UrlHelper
  include ActionView::Helpers::TextHelper

  attr_reader :comment, :account, :options, :upvoted_ids, :current_user

  def initialize(comment, options = {})
    @comment = comment.removed? ? RemovedCommentNull.new(comment) : comment
    @account = @comment.account.decorate
    @upvoted_ids = options[:upvoted_ids]
    @current_user = options[:current_user]
    @options = options
  end

  def linked_account_username
    if account.id.present?
      link_to(account.username_with_at, account.path)
    else
      account.username_with_at
    end
  end

  def vote_button_label
    pluralize(comment.votes_count, 'vote')
  end

  def show_parent_link?
    options.key?(:show_parent_link) && show_parent_link && comment.parent_id ? true : false
  end

  def show_story?
    options.key?(:show_story) && options[:show_story] ? true : false
  end

  def show_children?
    options.key?(:show_children) && options[:show_children] ? true : false
  end

  def show_reply_link?
    options.key?(:show_reply_link) ? options[:show_reply_link] : true
  end

  def show_edit?
    !comment.removed? && CommentPolicy.new(current_user, comment).edit?
  end

  def can_vote?
    !comment.removed? && current_user.present?
  end

  def upvoted?
    upvoted_ids.include? comment.id
  end

  def root_classes
    classes = []

    classes.push('comment--voted') if upvoted?
    classes.push('comment--removed') if comment.removed?

    classes
  end
end
