class Notifications::SendForComment < ActiveInteraction::Base
  object :comment

  def execute
    return if comment.parent.blank? && comment.story.account == comment.account
    return if comment.parent.present? && comment.parent.account == comment.account

    notifable = comment
    context = comment.story

    if comment.parent.present?
      event_key = 'comment_reply'
      recipient = comment.parent.account
    else
      event_key = 'story_reply'
      recipient = comment.story.account
    end

    CreateNotificationJob.call(
      event_key,
      author: comment.account,
      recipient: recipient,
      notifable: notifable,
      context: context
    )
  end
end
