class Comments::BroadcastChanges < ActiveInteraction::Base
  object :comment

  def execute
    ActionCable.server.broadcast 'updates_channel', {
      event: 'comments.updated',
      data: ActivityPub::CommentSerializer.new(comment).data
    }
  end
end
