class Stories::ToggleVote < ActiveInteraction::Base
  object :story
  object :account

  def execute
    if account.voted_on?(story)
      compose(Stories::Unvote, inputs)
    else
      compose(Stories::Vote, inputs)
    end
  end
end
