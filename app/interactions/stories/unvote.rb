class Stories::Unvote < ActiveInteraction::Base
  object :story
  object :account

  def execute
    vote = ::Vote.find_by(account: account, voteable: story)

    if vote.present?
      vote.destroy
      Accounts::UpdateKarmaJob.perform_later(story.account.id, 'Story', 'remove')
    end

    vote
  end
end
