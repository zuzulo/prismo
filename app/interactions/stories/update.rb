class Stories::Update < Stories::CreateUpdateBase
  object :story

  validates :url, url: { allow_blank: true }
  validate :url_or_description_required

  def execute
    story.title = title if title?
    story.tag_names = tags if tag_list?
    story.description = description if description?

    if url.present? && Stories::UpdatePolicy.new(account.user, story).update_url?
      story.url = url
    end

    if story.save
      after_story_save_hook(story)
    else
      errors.merge!(story.errors)
    end

    story
  end

  def persisted?
    true
  end

  private

  def after_story_save_hook(story)
    Stories::ScrapJob.perform_later(story.id) if story.url_changed?
    Stories::BroadcastChanges.run! story: story
    ActivityPub::UpdateDistributionJob.call_later(story) if story.local?
  end
end
