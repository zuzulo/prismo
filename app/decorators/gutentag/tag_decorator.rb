class Gutentag::TagDecorator < Draper::Decorator
  delegate_all

  def to_s
    "##{object.name}"
  end

  def path
    h.tag_stories_path(object.name)
  end
end
