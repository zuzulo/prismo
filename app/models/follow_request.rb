class FollowRequest < ApplicationRecord
  belongs_to :account
  belongs_to :target_account, class_name: 'Account'

  validates :account_id, uniqueness: { scope: :target_account_id }

  def authorize!
    account.follow!(target_account, uri: uri)
    destroy!
  end
end
