module Prismo::Exceptions
  class Error < StandardError; end
  class UnauthenticatedError < RuntimeError; end

  class UnexpectedResponseError < Error
    def initialize(response = nil)
      if response.respond_to? :uri
        super("#{response.uri} returned code #{response.code}")
      else
        super
      end
    end
  end
end
