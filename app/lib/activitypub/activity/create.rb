class ActivityPub::Activity::Create < ActivityPub::Activity
  SUPPORTED_TYPES = %w[Note].freeze

  def perform
    return if unsupported_object_type? || invalid_origin?(@object['id'])

    @story = find_story
    return if @story.blank?

    if in_reply_to_uri.present?
      process_comment
    else
      @story
    end
  end

  private

  def find_story
    case in_reply_to
    when Comment then in_reply_to.story
    when Story then in_reply_to
    end
  end

  def process_comment
    @comment = Comments::Create.run!(
      uri: @object['id'],
      url: object_url,
      account: @account,
      body: parsed_content,
      created_at: @object['published'],
      story_id: @story.id,
      parent_id: in_reply_to.is_a?(Comment) ? in_reply_to.id : nil,
      local: false,
      auto_vote: false
    )
  end

  def replied_to_status
    return @replied_to_status if defined?(@replied_to_status)
    @replied_to_status = in_reply_to_uri.blank? ? nil : story_from_uri(in_reply_to_uri)
  end

  def in_reply_to
    return nil if in_reply_to_uri.blank?
    ActivityPub::TagManager.instance.smart_uri_to_resource(in_reply_to_uri)
  end

  def in_reply_to_uri
    value_or_id(@object['inReplyTo'])
  end

  def text_from_name
    if @object['name'].present?
      @object['name']
    elsif name_language_map?
      @object['nameMap'].values.first
    end
  end

  def object_url
    return if @object['url'].blank?

    url_candidate = url_to_href(@object['url'], 'text/html')

    if invalid_origin?(url_candidate)
      nil
    else
      url_candidate
    end
  end

  def name_language_map?
    @object['nameMap'].is_a?(Hash) && !@object['nameMap'].empty?
  end

  def parsed_content
    return '' if @object['content'].blank?

    ReverseMarkdown.convert(@object['content']).chomp('')
  end

  def unsupported_object_type?
    @object.is_a?(String) || !supported_object_type?
  end

  def supported_object_type?
    equals_or_includes_any?(@object['type'], SUPPORTED_TYPES)
  end

  def invalid_origin?(url)
    return true if unsupported_uri_scheme?(url)

    needle   = Addressable::URI.parse(url).host
    haystack = Addressable::URI.parse(@account.uri).host

    !haystack.casecmp(needle).zero?
  end
end
