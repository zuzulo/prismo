class Accounts::StoriesController < ApplicationController
  layout 'application'

  before_action :set_account_upvoted_story_ids

  before_action { set_jumpbox_link(find_account) }

  def hot
    @account = find_account
    set_meta_tags @account.decorate
                          .to_meta_tags
                          .merge(title: "Hot stories by #{@account.decorate}")

    stories = StoriesQuery.new.hot
    stories = StoriesQuery.new(stories).for_account(@account)

    @stories = stories.page(params[:page])

    respond_to do |format|
      format.html { render :index }
      format.json do
        render json: ActivityPub::ActorSerializer.new(@account),
               with_context: true,
               content_type: 'application/activity+json'
      end
    end
  end

  def recent
    @account = find_account
    set_meta_tags @account.decorate
                          .to_meta_tags
                          .merge(title: "Recent stories by #{@account.decorate}")

    stories = StoriesQuery.new.recent
    stories = StoriesQuery.new(stories).for_account(@account)

    @stories = stories.page(params[:page])

    render :index
  end

  private

  def find_account
    @find_account ||= Account.local.find_by!(username: params[:username]).decorate
  end
end
