class Api::Ujs::AccountsController < Api::Ujs::BaseController
  def toggle_follow
    user_needed
    account = find_account
    authorize account

    if current_account.following?(account)
      UnfollowService.new(current_account.object, account).call
    else
      FollowService.new(current_account.object, account).call
    end

    locals = {
      presenter: FollowButtonPresenter.new(current_account.object, account)
    }

    render 'shared/_follow_btn', layout: false, locals: locals
  end

  private

  def find_account
    Account.local.find(params[:id])
  end
end
