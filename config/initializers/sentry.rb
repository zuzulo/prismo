Raven.configure do |config|
  config.environments = %w[production]
  config.release = Prismo::Version.to_s
end
