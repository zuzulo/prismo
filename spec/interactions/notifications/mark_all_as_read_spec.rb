require 'rails_helper'

describe Notifications::MarkAllAsRead do
  describe '#run' do
    subject { described_class.run account: account }

    let(:account) { create(:account) }
    let!(:notifications) { create_list(:notification, 2, seen: false, recipient: account) }
    let!(:other_user_notification) { create(:notification, seen: false) }

    it 'changes all the notification seen status to true' do
      expect { subject }
        .to change(account.notifications_as_recipient.seen, :count)
        .from(0).to(2)
    end

    it 'sends notification event to account user' do
      expect(NotificationsChannel)
        .to receive(:broadcast_to)
        .with(account.user, any_args)

      subject
    end
  end
end
