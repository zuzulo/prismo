keypair     = OpenSSL::PKey::RSA.new(2048)
public_key  = keypair.public_key.to_pem
private_key = keypair.to_pem

FactoryBot.define do
  factory :account do
    sequence(:username) { |i| "username#{i}" }
    public_key public_key
    private_key private_key

    trait :remote do
      domain 'example.com'

      after(:build) do |account|
        account.uri = "https://example.com/users/#{account.username}"
      end
    end
  end
end
