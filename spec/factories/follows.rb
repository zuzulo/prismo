FactoryBot.define do
  factory :follow do
    account
    association :target_account, factory: [:account]
  end
end
