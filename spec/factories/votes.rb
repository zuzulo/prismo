FactoryBot.define do
  factory :vote do
    account
    association :voteable, factory: [:story, :comment]
  end
end
