require 'faker'

FactoryBot.define do
  factory :comment do
    body Faker::Lovecraft.paragraph
    body_html 'Cached body html'
    local true

    account
    story

    trait :remote do
      local false
      uri 'https://mastodon.technology/users/username/statuses/123456'
      url 'https://mastodon.technology/users/username/statuses/123456'
      domain 'mastodon.technology'
    end
  end
end
