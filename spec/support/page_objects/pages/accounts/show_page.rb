require_relative '../../sections/story_row_section'
require_relative '../../sections/account_sub_nav_section'

class Accounts::ShowPage < SitePrism::Page
  set_url '/@{username}'
  set_url_matcher %r{\/@\w+\z}

  element :follow_button, 'a[data-controller="follow-btn"]'
  element :edit_profile_button, 'a.btn-edit-profile'

  section :sub_nav, AccountSubNavSection, '.account-head .tab'
  sections :stories, StoryRowSection, '.stories-list li.story-row'

  def click_follow_button
    follow_button.click
  end
end
