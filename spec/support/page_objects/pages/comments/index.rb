require_relative '../../sections/comment_section'

class Comments::IndexPage < SitePrism::Page
  set_url '/comments'
  set_url_matcher %r{\/comments}

  sections :comments, CommentSection, '.comment'
end
