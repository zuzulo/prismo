require_relative 'new_comment_form_section'
require_relative 'edit_comment_form_section'

class CommentSection < SitePrism::Section
  element :reply_btn, '.comment-reply-btn'
  element :edit_btn, '.comment-edit-btn'
  element :vote_button, '.comment-vote-btn'
  element :votes_count, '.comment-vote-btn'
  element :body, '.comment-body'

  section :new_comment_form, ::NewCommentFormSection, '.new_comment'
  section :edit_comment_form, ::EditCommentFormSection, 'form[data-mode="update"]'
  sections :child_comments, ::CommentSection, '.comment'

  def click_vote_button
    vote_button.click
  end

  def voted?
    root_element[:'data-voted'] == 'true'
  end
end
