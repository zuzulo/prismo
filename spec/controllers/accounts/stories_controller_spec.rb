require 'rails_helper'

describe Accounts::StoriesController, type: :controller do
  render_views

  let(:account) { create(:account) }

  describe 'GET #hot' do
    subject { get :hot, params: { username: account.username } }

    it 'renders successfull response' do
      subject
      expect(response).to be_success
    end

    context 'when json is requested' do
      subject { get :hot, params: { username: account.username }, format: :json }

      it 'renders successfull response' do
        subject
        expect(response).to be_success
      end
    end
  end

  describe 'GET #recent' do
    subject { get :recent, params: { username: account.username } }

    it 'renders successfull response' do
      subject
      expect(response).to be_success
    end
  end
end
