require 'rails_helper'

describe StoriesController, type: :controller do
  render_views

  let(:story) { create(:story) }

  describe 'GET #index' do
    subject { get :index }

    it 'renders successfull response' do
      subject
      expect(response).to be_success
    end
  end

  describe 'GET #recent' do
    subject { get :recent }

    it 'renders successfull response' do
      subject
      expect(response).to be_success
    end
  end

  describe 'GET #show' do
    subject { get :show, params: { id: story.id } }

    it 'renders successfull response' do
      subject
      expect(response).to be_success
    end
  end
end
