require 'rails_helper'

describe Stories::ScrapJob do
  describe '#perform_now' do
    subject { described_class.perform_now(story.id) }
    let(:link_body) { '' }

    before do
      stub_request(:get, 'https://example.com')
        .to_return(status: 200, body: link_body)
    end

    context 'when story has no url set' do
      let(:story) { create(:story, :text) }

      it { is_expected.to eq nil }
    end

    context 'when story has url set' do
      let(:story) { create(:story, :link, url: 'https://example.com') }

      it 'updates scrapped_at column' do
        expect { subject }.to change { story.reload.scrapped_at }
      end

      describe 'it saves page title' do
        let(:link_body) do
          '<title>Example page title</title>'
        end

        it do
          subject
          expect(story.reload.url_meta.title).to eq 'Example page title'
        end
      end

      describe 'it saves page description' do
        let(:link_body) do
          '<meta content="Example description" property="og:description">'
        end

        it do
          subject
          expect(story.reload.url_meta.description).to eq 'Example description'
        end
      end
    end
  end
end
